import hashlib
import xml.etree.ElementTree as ET
from typing import *
def hash_string(s: str):
    return hashlib.sha256(s.encode()).hexdigest()



def remove_namespace(doc, namespace):
    """Remove namespace in the passed document in place."""
    ns = u'{%s}' % namespace
    nsl = len(ns)
    id = '{http://www.w3.org/XML/1998/namespace}'
    idl = len(id)
    for elem in doc.iter():
        namespaced_attribs = list(filter(lambda x: x.startswith(id), elem.attrib.keys()))
        for attr in namespaced_attribs:
            elem.attrib[attr[idl:]] = elem.attrib[attr]
            del elem.attrib[attr]

        if elem.tag.startswith(ns):
            elem.tag = elem.tag[nsl:]



def handle_raw_trace(raw_trace):
    trace = []
    for raw_point in raw_trace.split(","):
        coords = raw_point.split()
        trace.append(tuple(map(float, coords)))
    return trace


def search_node(node: ET.Element, parents: Tuple[Tuple[str, str]], index: List[int], tracegroup_list: List[dict],
                trace_list: List[dict],
                annotation_list: List[dict], annotation_xml_list: List[dict]):
    node_id: str = hash_string(f"{node}{index[0]}")[:10]
    index[0] = index[0] + 1

    if node.tag == "annotationXML":
        row = {'node_id': node_id}
        row.update({k: v for k, v in node.attrib.items()})
        row['parents'] = parents
        annotation_xml_list.append(row)
    if node.tag == "annotation":
        row = {'node_id': node_id}
        row.update({k: v for k, v in node.attrib.items()})
        row['parents'] = parents
        row['text'] = node.text
        annotation_list.append(row)
    if node.tag == "traceGroup":
        row = {'node_id': node_id}
        row.update()
        row['parents'] = parents
        tracegroup_list.append(row)
        row['traceView'] = ""
        for c in node:
            if c.tag=="annotation":
                row.update({"annotation":c.text})
            if c.tag =="traceView":
                kv = {k: v for k, v in c.attrib.items()}
                if row['traceView']!="":
                    row['traceView']+=","
                row['traceView'] += kv["traceDataRef"]
    if node.tag == 'trace':
        row = {'node_id': node_id}
        row.update({k: v for k, v in node.attrib.items()})
        row['parents'] = parents
        row['trace'] = node.text
        trace_list.append(row)
    for child in node:
        search_node(child, parents + ((node.tag, node_id),), index, tracegroup_list, trace_list, annotation_list,
                    annotation_xml_list)


def get_trace_format(root: ET.Element, root_id, trace_format_list):
    trace_format_nodes = root.findall("traceFormat")
    for format_node in trace_format_nodes:
        row = {'file_root': root_id}
        if 'type' in format_node.attrib.keys():
            row['type'] = format_node.attrib['type']
        channels = format_node.findall('channel')
        trace_format = {}
        for i, channel in enumerate(channels):
            trace_format[channel.attrib['name'].lower()] = i
        row['format'] = trace_format
        trace_format_list.append(row)


def parse_file(file, index, tracegroup_list: List[dict], trace_list: List[dict], annotation_list: List[dict],
               annotation_xml_list: List[dict], trace_format_list: List[dict], root_file_list: List[dict]):
    root = ET.parse(file).getroot()


    remove_namespace(root, u'http://www.w3.org/2003/InkML')
    root_id = hash_string(f'root{index[0]}')[:10]
    root_file_list.append({'node_id': root_id, 'file_path': file})
    get_trace_format(root, root_id, trace_format_list)
    for node in root:
        search_node(node, (('root', root_id),), index, tracegroup_list, trace_list, annotation_list, annotation_xml_list)

def simple_structure_parsing(file,multiplierCoord,removeDuplicatedPoint:bool)-> List[Tuple[List[List[Tuple[float,float,float,float]]],str]]:
    tracegroup_list: List[dict] = []
    trace_list: List[dict] = []
    annotation_list: List[dict] = []
    annotation_xml_list: List[dict] = []
    trace_format_list: List[dict] = []

    root_file_list: List[dict] = []
    # fill the lists
    parse_file(file, [0], tracegroup_list, trace_list, annotation_list, annotation_xml_list, trace_format_list,
               root_file_list)
    gestures:List[Tuple[List[List[Tuple[float,float,float,float]]],str]] = [] #Gestures[(Strokes[Path[x y pressure time]],classname)]

    for tracegroup in tracegroup_list:
        tracesIdCorresp = tracegroup["traceView"].split(",")  #
        GT = tracegroup["annotation"]
        traces:List[List[Tuple[float,float,float,float]]] = [] #Strokes[Path[x y pressure time]]

        for id,trace in enumerate(trace_list[int(tracesIdCorresp[0]):int(tracesIdCorresp[-1])+1]):
            assert trace["id"] == tracesIdCorresp[id]
            # tracing is : xPos1 yPos1 Pressure1 Time1, xPos2 yPos2 Pressure2 Time2....
            tracing = trace["trace"]
            tracePoints = tracing.split(",") # List[str]
            if removeDuplicatedPoint:
                tracePointsWithoutDuplication = []
                [tracePointsWithoutDuplication.append(n) for n in tracePoints if n not in tracePointsWithoutDuplication]
                tracePoints = tracePointsWithoutDuplication
            tracePoints = map(lambda s : s.split(),tracePoints) #List[List[str]]
            tracePoints:List[Tuple[float,float,float,float]] = list(
                map(
                    lambda e: (float(e[0])*multiplierCoord,float(e[1])*multiplierCoord,float(e[2]),float(e[3])),
                    tracePoints))

            traces.append(tracePoints)
        gestures.append((traces,GT))
    return gestures
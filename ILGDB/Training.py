#%% md

# Training model

#%%

import os
import random
from datetime import datetime
from math import ceil
from typing import List
import wandb
from wandb.keras import WandbCallback
import tensorflow as tf
import numpy as np
from Model.ModelEarly2D import ModelEarly2D
import tensorflow_addons as tfa

# pathDB = "C:\\workspace2\\Datasets\\2D\\MTG\\MTGSetB\\"
pathDB = "/srv/tempdd/wmocaer/data/2D/ILGDB/"
separator = "/"
pathPreprocessedData = pathDB+"PreprocessedUntrimmedX5_withValid/Train/"
pathPreprocessedDataValid = pathDB+"PreprocessedUntrimmedX5_withValid/Valid/"
pathCount = pathDB+"PreprocessedUntrimmedX5_withValid/Traincount"
pathCountValid = pathDB+"PreprocessedUntrimmedX5_withValid/Validcount"
pathLog =pathDB+"Log/"
physical_devices = tf.config.experimental.list_physical_devices('GPU')
assert len(physical_devices) > 0, "Not enough GPU hardware devices available"

#config = tf.config.experimental.set_memory_growth(physical_devices[0], True)

f = open("ILGDB/config.txt",'r')
configParams = f.readlines()
f.close()
configParams = eval("\n".join(configParams))

multiplierCoord = configParams["multiplierCoord"]
thresholdCuDi = configParams["thresholdCuDi"]# value in pixel
dimensionsImage,canal = np.array(configParams["dimensionsOutputImage"]),2
dataset="ILGDB"

#%% md

### Define loss and metrics

#%%

couverture = 0.6
nbClass=21
def getWo0TruePred(y_true,y_pred):
    #y_true: [batch,nbSeg,1]
    #y_pred: [batch,nbSeg,nbClass]
    y_pred = tf.boolean_mask(y_pred, tf.squeeze(y_true!=-1,axis=-1))
    y_true = tf.boolean_mask(y_true, y_true!=-1)

    y_true = tf.one_hot(tf.cast(y_true,tf.int32),depth=nbClass,dtype=tf.float32)


    originalCount = tf.cast(tf.shape(y_true)[0],tf.float32)
    #select the accepted predictions
    y_pred_accepted_mask = tf.greater(y_pred[:,0],0.5) # y_pred[:,0] correspond to the selective gate of each elem
    # tf.print("shape y_pred_accepted_mask",tf.shape(y_pred_accepted_mask))
    # print("shape y_pred_accepted_mask",y_pred_accepted_mask)
    # print("shape y_true",y_true)
    # tf.print("shape y_true",tf.shape(y_true))
    #mask : [batch,seq]
    y_predWo0 = y_pred[:,1:]
    y_predWo0_accepted = tf.boolean_mask(y_predWo0,y_pred_accepted_mask,axis=0) # don't take the prediction rejected
    y_trueWo0_accepted = tf.boolean_mask(y_true,y_pred_accepted_mask,axis=0) # [batch*seg,nbClass]
    # tf.print("shape y_trueWo0_accepted",tf.shape(y_trueWo0_accepted))
    # print("shape y_trueWo0_accepted",y_trueWo0_accepted)
    newCount = tf.cast(tf.shape(y_trueWo0_accepted)[0],tf.float32)
    return y_trueWo0_accepted,y_predWo0_accepted,originalCount,newCount

catCroEnt = tf.keras.losses.CategoricalCrossentropy(reduction=tf.keras.losses.Reduction.AUTO)
catSparseCroEnt = tf.keras.losses.SparseCategoricalCrossentropy(reduction=tf.keras.losses.Reduction.AUTO)
def lossFGWithReject(y_true,y_pred,lambdaHyper):
    #y_true: [batch,nbSeg,nbClass]
    #y_pred: [batch,nbSeg,nbClass]
    weight = tf.cast(tf.not_equal(y_true[:, :, 0], -1), tf.float32)  # correspond to non-0 actions
    y_true = tf.keras.activations.relu(y_true) # to eliminate the -1 padding
    # weight = weight * (weightOfBG - 1) + 1

    # y_trueAction = tf.boolean_mask(y_true, y_true_accepted_mask, axis=0)
    # loss of selective Net :
    y_true = tf.one_hot(tf.squeeze(tf.cast(y_true,tf.int32),axis=-1),depth=nbClass,dtype=tf.float32)
    # tf.print("sahpeFGs " ,tf.shape(y_true),tf.shape(y_pred))

    loss = catCroEnt(
        tf.repeat(y_pred[:,:,:1], nbClass, axis=2) * y_true[:,:,:] , # g(x)*ytrue * log(pred)
        y_pred[:,:,1:],weight) # y_pred[:, :,:1] is confusion (index 0)
    # tf.print("lossFG" ,loss)

    # y_predAction = tf.ragged.boolean_mask(y_pred, y_true_accepted_mask)
    #
    # average = tf.cast(tf.reduce_sum(y_predAction[:,:,0], axis=1),tf.float32)  # 1 value is not
    # # avoid NaN when its sequences with only zeros
    # average = tf.math.divide_no_nan(average,tf.cast(y_predAction.row_lengths(axis=1),dtype=tf.float32))
    y_true_accepted_mask = tf.not_equal(y_true[:, :, 0], -1)#only action # one hot here

    y_predAction = tf.ragged.boolean_mask(y_pred, y_true_accepted_mask)
    average = tf.cast(tf.reduce_sum(y_predAction[:,:,0], axis=1),tf.float32)
    average = tf.math.divide_no_nan(average,tf.cast(y_predAction.row_lengths(axis=1),dtype=tf.float32))
    lossCouverture = tf.reduce_mean(lambdaHyper * tf.maximum( couverture- average, 0) ** 2)
    #
    # loss += lambdaHyper*tf.maximum(couverture-tf.reduce_mean(y_predAction[:,0]),0)**2
    # tf.print("lossFGaft" ,loss)
    return loss+lossCouverture

def lossHAux(y_true,y_pred):
    # tf.print("sahpes " ,tf.shape(y_true),tf.shape(y_pred))
    weight = tf.cast(tf.not_equal(y_true[:, :, 0], -1), tf.float32)  # correspond to non-0 actions
    loss = catSparseCroEnt(tf.keras.activations.relu(y_true),y_pred,weight) # relu juste to eliminate -1
    # tf.print("lossAUX" ,loss)
    return loss

def TAR_allValues(y_true,y_pred):
    y_true, y_pred, originalCount, newCount = getWo0TruePred(y_true,y_pred)
    return tf.metrics.categorical_accuracy(y_true,y_pred)*newCount/originalCount

def FAR_allValues(y_true,y_pred):
    y_true, y_pred, originalCount, newCount = getWo0TruePred(y_true,y_pred)
    return (1-tf.metrics.categorical_accuracy(y_true,y_pred))*newCount/originalCount

def TAR_FAR_Diff_allValues(y_true,y_pred):
    return TAR_allValues(y_true,y_pred) - 3*FAR_allValues(y_true,y_pred)
def RejectRate_allValues(y_true,y_pred):
    y_true, y_pred, originalCount, newCount = getWo0TruePred(y_true,y_pred)
    return (originalCount-newCount)/originalCount
def lossCTC(y_true,y_pred):

    y_true_argmax = y_true[:,:,0]
    # tf.print("y_true_argmax",y_true_argmax)
    y_true_argmax_ragged = tf.ragged.boolean_mask(y_true_argmax, y_true_argmax!=-1)
    length = y_true_argmax_ragged.row_lengths(axis=1) #<tf.Tensor: shape=(2,), dtype=int64, numpy=array([7, 8], dtype=int64)>
    # tf.print("length",length)

    shapeOfPred = tf.shape(y_pred)
    loss = tf.nn.ctc_loss(labels=y_true_argmax+1,logits=y_pred,#should be logits (before softmax)
                   label_length= length,
                   logit_length=tf.repeat(shapeOfPred[1],shapeOfPred[0]),
                   logits_time_major=False,blank_index=0)
    # tf.print("lossCtc",tf.reduce_mean(loss))
    return tf.reduce_mean(loss)

#%%

# trainPart = 0.85

# trainFiles = os.listdir(pathPreprocessedData)
#
# nbTrain = int(trainPart*len(trainFiles))
# nbValid = len(trainFiles)-nbTrain
# train = trainFiles[0:nbTrain]
# valid = trainFiles[nbTrain:]
# random.shuffle(train)
fCount = open(pathCount, "r")
countTrain = int(fCount.readlines()[0])
fCount.close()

fCount = open(pathCountValid, "r")
countValid = int(fCount.readlines()[0])
fCount.close()


#%% md

### Define hyper-parameters

#%%

nbTrain = countTrain
nbValid = countValid
dilatationRates = [1, 2, 4, 8, 16, 1, 2, 4, 8, 16]
config = {    "multiplierCoord":multiplierCoord,
              "treshCudi":thresholdCuDi,
                "dimension":[dimensionsImage[0],dimensionsImage[1]],
               "batchSize": 10,
               "lambdahyper": 32,
                "couverture":couverture,
              "weightLoss1":0.5,
              "weightCTC": 0.01,
              "learning_rate":0.003,
               "doGlu":False,
               "dropoutVal":0.1,
               "denseSize":100,
               "denseDropout":0.3,
               "nbFeatureMap":30,
               "dilatationRates":dilatationRates,
               "maxPoolSpatial":True,
               "poolSize":(1,3,3),
                "nbDenseLayer":1,
               "train_size":nbTrain,
               "val_size":nbValid,
               }


#%% md

### Initialize wandb

#%%

tags = [dataset,"TempRejectPerSeq"]
wandBRun = wandb.init(project="precoce2d-deep-wavenet",entity="intuidoc-gesture-reco",save_code=True,reinit=True,tags=tags,config=config)
config = wandb.config # will set new hyperparameters when sweep used
wandb.summary["dataset"]=dataset
wandb.summary["canaux"]=canal
wandb.summary["nbClass"]=nbClass
wandb.summary["receptiveField"]=sum(config.dilatationRates)
wandb.summary["nbConvLayers"]=len(config.dilatationRates)

print("WANDB run name = "+ wandBRun.name)
wandBRunDir = wandBRun.dir

#%% md

### Define the model

#%%

metrics = [TAR_allValues,FAR_allValues,RejectRate_allValues,TAR_FAR_Diff_allValues]
model = ModelEarly2D(nbClass=nbClass,boxSize=(dimensionsImage[0],dimensionsImage[1],canal),
                     doGLU=config.doGlu,dropoutVal=config.dropoutVal,denseNeurones=config.denseSize,
                     denseDropout=config.denseDropout,nbFeatureMap=config.nbFeatureMap,
                     dilatationsRates=config.dilatationRates,maxPoolSpatial=config.maxPoolSpatial,
                     poolSize=config.poolSize,poolStrides=config.poolSize)
opti = tf.keras.optimizers.Adam(learning_rate=config.learning_rate)
model.compile(opti, loss=[lambda x,y:lossFGWithReject(x,y,config.lambdahyper),lossHAux,lossCTC], loss_weights=[config.weightLoss1,1-config.weightLoss1,config.weightCTC],
              metrics=[metrics,[],[]])

#%% md

### Prepare input data

#%%


def AugmentationRotation(input1, GT):
    """

    :param input1: input, dim [seq, dimensionsImage[0], dimensionsImage[1], canal(2)]
    :param GT: (input2,input2,input3,conca)
    :return:
    """
    degree = tf.random.normal(shape=(),mean=0.0, stddev=10, dtype=tf.float32)
    rotation = degree * np.pi / 180.
    input1 = tfa.image.rotate(input1, rotation, interpolation="nearest")
    return input1,GT

def configureDataset(dataset:tf.data.Dataset,train=True):

    # dataset = dataset.take(nbTrain)
    # datasetValid = dataset.skip(nbTrain)
    if train:
        dataset = dataset.map(AugmentationRotation, num_parallel_calls=tf.data.AUTOTUNE)

        dataset = dataset.shuffle(buffer_size=nbTrain, reshuffle_each_iteration=True)

    toPad = (
        (tf.constant(0,dtype=tf.float32)),
        (tf.constant(-1,dtype=tf.int32), tf.constant(-1,dtype=tf.int32), tf.constant(-1,dtype=tf.int32))
    )
    # if is4D:
    #     input_shape = tf.TensorShape(
    #         [None, dimensionsImage[0], dimensionsImage[1], dimensionsImage[2],])
    # else:
    input_shape = tf.TensorShape([None, dimensionsImage[0], dimensionsImage[1],2])

    output_shapes = (input_shape,
                     (tf.TensorShape([None, 1]), tf.TensorShape([None, 1]), tf.TensorShape([None, 1])))
    dataset = dataset.padded_batch(config.batchSize, padded_shapes=output_shapes,
                                           padding_values=toPad)



    dataset = dataset.repeat()
    return dataset



datasetTrain = tf.data.experimental.load(pathPreprocessedData)
datasetValid = tf.data.experimental.load(pathPreprocessedDataValid)


datasetTrain,datasetValid = configureDataset(datasetTrain),configureDataset(datasetValid,False)

#%% md

### Prepare callbacks for training

#%%

date=datetime.now().strftime("%Y%m%d-%H%M%S")
pathWeight = pathLog+date+separator+"Weights"
if not os.path.exists(pathLog):
    os.mkdir(pathLog)
os.mkdir(pathLog+date)
os.mkdir(pathWeight)
os.mkdir(pathLog+date+separator+"TensorBoard")
earlyStop = tf.keras.callbacks.EarlyStopping(monitor="val_output_1_TAR_FAR_Diff_allValues", verbose=1, patience=100, mode='max')
checkpoint =  tf.keras.callbacks.ModelCheckpoint(pathLog+date+separator+"Weights"+separator+"model", monitor="val_output_1_TAR_FAR_Diff_allValues", verbose=1, save_best_only=True,
                                     mode='max')
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=pathLog+ date+separator+"TensorBoard"+separator+"training", histogram_freq=1)
callbacks = [earlyStop,checkpoint,tensorboard_callback,WandbCallback()]



#%% md

# Fitting

#%%

history = model.fit(datasetTrain, epochs=3000, steps_per_epoch=int(ceil(nbTrain // config.batchSize)), verbose=2,
                        validation_data=datasetValid, validation_steps= int(ceil(nbValid / config.batchSize)),
                        callbacks=callbacks)  # val_size / batchSize
print("fitted ! ", len(history.history['loss']), " epochs")

#%% md

### add some values in wandb

#%%

import tensorflow.keras.backend as K
trainable_count = np.sum([K.count_params(w) for w in model.trainable_weights])
wandb.log({"TrainableParams":trainable_count})
try:
    def myprint(s):
        print(s)
        with open(pathWeight+separator+"totalWeigth.txt", 'a+') as f:
            f.write(s+"\n")

    model.summary(print_fn=myprint)
except Exception as e:
    print("Problem with weight calculation 2")
    print(e)
# copyfile(ModelEarly2D.py,wandBRunDir+"ModelEarly2D.py")
wandb.finish()

#%%




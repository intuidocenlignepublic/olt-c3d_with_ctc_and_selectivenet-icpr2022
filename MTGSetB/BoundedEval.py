from typing import List, Tuple

import numpy as np
# def BoundedActionTemporalIoUMetric( predictions: np.ndarray, rejections: np.ndarray,
#                           strategy, nbClass: int,canCorrect,minCouverture:float,TOLERANCE:int,boundsGT: List[Tuple[int, int, int]]):
#     """
#
#     :param pathCuDiSplit: the path to find the CuDi details (number of temporal frames per CuDi frames)
#     :param pathLabelOriginal: the path to find the original label description per sequence ( without seq file name)
#     :param nbFrameBefore: the nubmer of frame before the action point to Evaluate
#     :param stategy: the strategy to decide
#     :param file: the name of the sequence
#     :param predictions: list[int] (CuDi space)
#     :param rejection: List[0<float<1] represent the confidence, per prediction
#     :param GT: the ground truth per CuDi frame
#     :param nbClass: the number of classes
#     :param TOLERANCE: in number of frame the tolerance allowed to detect an action before it starts,
#                 should depends of quality of annotation
#     :return:TP,FP,MatConf,nbPerclasses,Precision_c,Recall_c,earliness,NLToD_c,
#     #TPAll,FPAll,Precision,Recall,NLToD,nbActionsTotalGT,TrueAcceptAt,FalseAcceptAt,detailedResult
#                 resultsDetailed[number of actionPoints,nbFrameBefore+1] :
#                         -5 : the corresponding frame is before the action start
#                         -4 : the corresponding frame is not an action (0)
#                         -3 : no CuDi frame in this ratio
#                         -2 : FP or FN
#                         -1 : no starts found between action start and observed frame
#                         0  : Reject prediction
#                         1  : TP
#     """
#
#     assert len(predictions) == len(rejections)
#     # -1 to match with temporal index
#     # actionsId_Points = getId_Start_ActionPoints_End(pathLabelOriginal, file)
#
#     #start,end,classID
#     # boundsGT: List[Tuple[int, int, int]] = list(map(lambda classe_start_AP_end: (classe_start_AP_end[1],classe_start_AP_end[3],classe_start_AP_end[0]),actionsId_Points ))
#
#     # strategy must emit 0 action class bound
#     bounds: List[Tuple[int, int, int]]
#     bounds,_ = strategy(predictions, rejections)  # return start, end, class id
#
#     #TP,FP,MatConf,nbPerclasses,Precision_c,Recall_c,earliness,NLToD_c,
#     #TPAll,FPAll,Precision,Recall,NLToD,nbActionsTotalGT,TrueAcceptAt,FalseAcceptAt,detailedResult
#     return Evaluate(bounds,boundsGT,canCorrect,minCouverture,nbClass,TOLERANCE)




def IoU_deb(start, end, startGT, endGT):
    """

    :param start: pred
    :param end:  pred
    :param startGT: GT
    :param endGT: GT
    :return: Intersection over Union, considering union from start and not from startGT
    we dont want to penalize the non-earliness on this criteria
    """

    maxiStart = max(start,startGT)
    miniEnd = min(end,endGT)

    if miniEnd < maxiStart: #no overlap
        return 0
    intersection = miniEnd-maxiStart+1

    areaPred = (end-start)+1
    assert max(startGT,start)<=endGT
    areaGTTroncated = (endGT-max(startGT,start))+1 # troncated with start of prediction


    union = areaPred+areaGTTroncated-intersection
    return intersection/union


def BoundedActionTemporalIoUMetric(boundsPredictions:List[Tuple[int, int, int]] ,boundsGTTemporal:List[Tuple[int, int, int]] ,canCorrect:bool
             ,minCouverture:float, nbClass:int):
    """


    :param boundsPredictions: List[start, end, class id], temporal domain
    :param boundsGTTemporal: List[start, end, class], temporal domain
    :param canCorrect: true if allow correction by the system, if the first detection was a False Positive
    :param minCouverture: the minimum couverture to be match the GT
    :param nbClass: the number of class
    :return:
    detailedResult : 1 :TP
                     0 : reject
                    -1 : FP on action (wrong class)
                    -2 : FP on action (IoU <Mini)
                    -3 : FP on BG/flag taken
    """

    boundsPredictions.sort(key=lambda start_end_classe:start_end_classe[1]) # in place
    boundsGTTemporal = boundsGTTemporal.numpy().tolist()
    boundsGTTemporal.sort(key=lambda start_end_classe:start_end_classe[1]) # in place


    flags = [0]*len(boundsGTTemporal)

    TP = np.zeros([nbClass+1])
    FP = np.zeros([nbClass+1])
    MatConf = np.zeros([nbClass+1,nbClass+1]) # +1: non-action,
    earliness = [[] for _ in range(nbClass+1)]
    nbPerclasses = np.zeros([nbClass+1])
    TrueAcceptAt = []
    FalseAcceptAt = []

    detailedResult = []

    for idGT, (classIdGT,startGT, endGT) in enumerate(boundsGTTemporal):
        nbPerclasses[classIdGT] += 1

    for idPrediction,(classId,start,end)in enumerate(boundsPredictions):
        classId = int(classId)
        # necessary one result !=0 because the gestures are chained
        gtIndex = int(np.argmax(list(map(lambda clID_startlab_endlab : IoU_deb(start,end,clID_startlab_endlab[1],clID_startlab_endlab[2]), boundsGTTemporal))))
        classIdGT,startGT,endGT = boundsGTTemporal[gtIndex]
        if flags[gtIndex]==0 and classId==classIdGT and IoU_deb(start,end,startGT,endGT)>minCouverture:
            flags[gtIndex] = 1
            TP[classIdGT] += 1
            MatConf[classIdGT][classIdGT] += 1

            #length for NDToD
            #endGT-startGT+1-2 for MTG because can have 2 black frames,
            # endGT-startGT for ILG because have overlap with next gesture in GT
            # nltodVal = min(1,max(0,(start-startGT+1))  /(endGT-startGT+1))
            nltodVal = min(1,max(0,(start-startGT+1))  /(endGT-startGT+1-2))

            earliness[classIdGT].append(  nltodVal )
            TrueAcceptAt += [nltodVal]
            detailedResult.append(1)
        else:
            if not canCorrect:
                flags[gtIndex] = 1
            FP[classId] += 1
            MatConf[classIdGT][classId] += 1
            # nltodVal = min(1,max(0,(start-startGT+1))  /(endGT-startGT+1))
            nltodVal = min(1,max(0,(start-startGT+1))  /(endGT-startGT+1-2))
            FalseAcceptAt += [nltodVal]

            if (classId!=classIdGT):
                detailedResult.append(-1)
                        # print("class != ",classId,classIdGT)
            else:
                detailedResult.append(-2)
                        # print("IOU < ",IoU_deb(start,end,startGT,endGT))

        #
        # if not found:
        #     FP[classId] += 1
        #     MatConf[-1][classId] += 1
        #     print("FP segmentPred",idPrediction,"on",len(boundsPredictions))
        #     detailedResult.append(-3)


    #--Per class
    Precision_c = np.divide(TP, TP + FP, out=np.zeros_like(TP), where=TP + FP != 0)
    Recall_c = np.divide(TP, nbPerclasses, out=np.zeros_like(TP), where=nbPerclasses != 0)
    NLToD_c = np.array([np.average(elem) for elem in earliness])
    #-- Total, micro average
    TPAll = np.sum(TP)
    FPAll = np.sum(FP)
    Precision = np.divide(TPAll, TPAll + FPAll, out=np.zeros_like(TPAll), where=TPAll + FPAll != 0)
    Recall = TPAll / (len(boundsGTTemporal))
    flat_earliness = [item for sublist in earliness for item in sublist]
    NLToD = np.average(np.array(flat_earliness))

    return TP,FP,MatConf,nbPerclasses,Precision_c,Recall_c,earliness,NLToD_c, \
           TPAll,FPAll,Precision,Recall,NLToD,len(boundsGTTemporal),TrueAcceptAt,FalseAcceptAt, detailedResult

